import sys
#from netCDF4 import Dataset
import xarray as xr

fpath = sys.argv[1]

#f = Dataset(fpath, 'r')
#time = f.variables['time'][:]
#tstr = str(time[0])
#year = tstr[:4]
#month = tstr[4:6]
#day = tstr[6:8]

ds = xr.open_dataset(fpath)
tstr = ds.tc_startdate
year = tstr.split('T')[0].split('-')[0]
month = tstr.split('T')[0].split('-')[1]
day = tstr.split('T')[0].split('-')[2]
month = str(int(month))

print(year, month, day)
