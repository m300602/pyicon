#!/bin/bash
#SBATCH --job-name=pyicon_qp
#SBATCH --time=00:30:00
#SBATCH --output=log.o-%j.out
#SBATCH --error=log.o-%j.out
#SBATCH --ntasks=1
#SBATCH --partition=compute,compute2
#SBATCH --account=mh0033

module list
source ./conda_act_dwd_pyicon_env.sh
which python

rand=$(cat /dev/urandom | tr -dc 'A-Z' | fold -w 3 | head -n 1)

path_pyicon=`(cd .. && pwd)`"/"
config_file="./config_qp_${rand}.py"
qp_compare="${path_pyicon}pyicon/quickplots/qp_compare_atm_dwd.py"
path_nc="/hpc/uwork/tvpham/pyicon/netcdf/"

cat > ${config_file} << %eof%
# --- path to quickplots
path_quickplots = '/hpc/uwork/tvpham/pyicon/all_qps/'

# --- set this to True if the simulation is still running
omit_last_file = False

# --- do ocean and/or atmosphere plots
do_atmosphere_plots = True
do_conf_dwd         = True
do_djf              = False
do_jja              = False
do_ocean_plots      = False

# --- grid information
gname     = 'R2B4-R2B4'
lev       = 'L40'
gname_atm = 'r2b4_atm_r0012'
lev_atm   = 'L90'

# --- path to interpolation files
path_grid        = '/hpc/uwork/icon-sml/pyICON/grids/'+gname+'/'
path_grid_atm    = '/hpc/uwork/icon-sml/pyICON/grids/'+gname_atm+'/'
path_ckdtree     = path_grid+'/ckdtree/'
path_ckdtree_atm = path_grid_atm+'/ckdtree/'

# --- grid files and reference data
path_pool_oce       = '/hpc/uwork/icon-sml/pyICON/grids/'
gnameu = gname.split('_')[0].upper()
fpath_tgrid                 = path_grid + gname+'_tgrid.nc'
fpath_tgrid_atm             = path_grid_atm + gname_atm+'_tgrid.nc'
fpath_ref_data_atm          = path_grid_atm + 'era5_pyicon_2001-2010_1.5x1.5deg.nc'
fpath_ref_data_atm_djf      = path_grid_atm + 'era5_pyicon_2001-2010_djf_1.5x1.5deg.nc'
fpath_ref_data_atm_jja      = path_grid_atm + 'era5_pyicon_2001-2010_jja_1.5x1.5deg.nc'
fpath_ref_data_atm_rad      = path_grid_atm + 'ceres_pyicon_2001-2010_1.5x1.5deg.nc'
fpath_ref_data_atm_rad_djf  = path_grid_atm + 'ceres_pyicon_2001-2010_djf_1.5x1.5deg.nc'
fpath_ref_data_atm_rad_jja  = path_grid_atm + 'ceres_pyicon_2001-2010_jja_1.5x1.5deg.nc'
fpath_ref_data_atm_prec     = path_grid_atm + 'gpm_pyicon_2001-2010_1.5x1.5deg.nc'
fpath_ref_data_atm_prec_djf = path_grid_atm + 'gpm_pyicon_2001-2010_djf_1.5x1.5deg.nc'
fpath_ref_data_atm_prec_jja = path_grid_atm + 'gpm_pyicon_2001-2010_jja_1.5x1.5deg.nc'
fpath_fx                    = path_grid + 'oce_fx.19600102T000000Z.nc'

# --- nc file prefixes ocean
oce_def     = '_oce_def'
oce_moc     = '_oce_moc'
oce_mon     = '_oce_mon'
oce_ice     = '_oce_ice'
oce_monthly = '_oce_dbg'

# --- nc file prefixes atmosphere
atm_2d      = '_atm_2d_ml'
atm_3d      = '_atm_3d_ml'
atm_mon     = '_atm_mon'

# --- nc output
save_data = False
path_nc = "${path_nc}"

# --- time average information (can be overwritten by qp_compare call)
tave_ints = [
#['1630-02-01', '1640-01-01'],
['4450-02-01', '4500-01-01'],
]
ave_freq = 12

# --- decide if time-series (ts) plots are plotted for all the 
#     available data or only for the intervall defined by tave_int
use_tave_int_for_ts = True

# --- what to plot and what not?
# --- not to plot:
red_list = ['atm_toa_sou_diff',  'atm_toa_sou_bias1', 'atm_toa_sou_bias2', 'atm_toa_sou_rmse1', 'atm_toa_sou_rmse2', 'atm_toa_sou_rmse_diff']
# --- to plot:
#green_list = ['']
%eof%

# --- start qp_compare
startdate=`date +%Y-%m-%d\ %H:%M:%S`

run1="tune160"
run2="tune162"
path_data1="/hpc/uwork/tvpham/ICON-CLM_SP_uwork/chain/scratch/${run1}/output/icon"
path_data2="/hpc/uwork/tvpham/ICON-CLM_SP_uwork/chain/scratch/${run2}/output/icon"
rm -r /hpc/uwork/tvpham/pyicon/all_qps/qp-${run2}-${run1}
python -W ignore -u ${qp_compare} --batch=True ${config_file} --path_data1=$path_data1 --path_data2=$path_data2 --run1=$run1 --run2=$run2 --tave_int='2001-01-01,2002-01-01'

enddate=`date +%Y-%m-%d\ %H:%M:%S`
chmod -R o+rx /hpc/uwork/tvpham/pyicon/all_qps/qp-${run2}-${run1}
rm ${config_file}

echo "--------------------------------------------------------------------------------"
echo "Started at ${startdate}"
echo "Ended at   ${enddate}"

