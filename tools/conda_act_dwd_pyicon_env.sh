#!/bin/bash

# path to my virtual environment
path_myvenv=$WORK/myvenv

# --- add pyicon to PYTHONPATH
PYICON_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"  #problem: takes directory of source script as base
#PYICON_PATH="$( cd "$(pwd)/.." >/dev/null 2>&1 && pwd )"  # problem: takes directory as base from where this script is called
export PYTHONPATH="${PYICON_PATH}"
# use this if you have a harmless PYTHONPATH which you want to keep
#export PYTHONPATH="${PYICON_PATH}:${PYTHONPATH}"
echo "PYTHONPATH was modified to:" 
echo "${PYTHONPATH}"
echo ""

# --- activate conda environment
source ${path_myvenv}/bin/activate
echo "Activated environment: " ${path_myvenv}/bin/activate

# --- print some information
python_path=`which python`
echo "Active python:"
echo "${python_path}"
