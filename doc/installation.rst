Installation of pyicon
======================

Meeting requirements on Levante
--------------------------------

Activate a suitable python environment. E.g.::

   module load python3/2023.01-gcc-11.2.0

Other supported python environments are: ``python3/2022.01-gcc-11.2.0(default)``,  ``python3/2023.01-gcc-11.2.0``, and ``python3/unstable``.

Some requirements are not automatically met by the python environments provided on Levante (e.g. `healpy`). If you recognize that 
a package is missing, please install either from the shell with::

  pip install package_name

Or within jupyter notebook with::

  %pip install package_name

(If this was succesful you can skip the next section.)

Meeting requirements on an arbitrary system
-------------------------------------------

(If you are working on Levante, you can skip this section and see the section above.)

pyicon is developed for python 3.8 or later. Other versions might work as well but are not supported so far.
Furthermore, a list of modules needed can be found in ``pyicon/requirements.txt`` and the latest ``pyicon/ci/requirements*.yml`` file. Some key modules are:

  * numpy, scipy (calculations)
  * matplotlib, cartopy (plotting)
  * netcdf4, xarray (reading / writing netcdf data)
  * ipython, jupyter (for running python)
  * mpi4py, dask (for parallel computing and distributed memory)

With conda you can generate a suitable python environment with::

  conda env create -f pyicon/ci/requirements_py39.yml

Download and installation
------------------------

Download pyicon (e.g. into your home directory)::

  git clone https://gitlab.dkrz.de/m300602/pyicon.git

There are two different options to enable including pyicon:

1. Installation via pip::
  
  cd pyicon

  pip install -e ./

2. Add pyicon to you PYTHONPATH variable. Therefore, add to your ``.profile`` file the following line (assuming pyicon is installed into the home directory)::
  
  export PYTHONPATH="${HOME}/pyicon:${PYTHONPATH}"

Check if pyicon is running by executing::

  python -c "import pyicon"
 
Pyicon is loaded successfully once you see the line: "----Pyicon was loaded successfully." 

