.. _pyicon-tools:

pyicon-tools
============

Pyicon-tools aims to facilitate working with ICON data by allowing to perform
specific tasks directly from the command line. Some promenent tools are:

  * ``pyic_fig``
  * ``pyic_sec``
  * ``pyic_tave``

