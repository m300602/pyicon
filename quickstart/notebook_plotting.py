# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

import xarray as xr
import cartopy.crs as ccrs
import pyicon as pyic

with open('./path_data.txt') as f:
    path_data = f.read()[:-1]
fpath_fx = f'{path_data}/r2b4_oce_r0004/r2b4_oce_r0004_L40_fx.nc'

ds = xr.open_mfdataset(f'{path_data}/outdata/*oce*3d*.nc')

# ## Horizontal plots

ds.to.isel(depth=0, time=0).pyic.plot()

# ## Sections and global zonal averages

ds.to.isel(time=0).pyic.plot_sec(section='170W')

ds.to.isel(time=0).pyic.plot_sec(section='gzave')

# ## Meridional overturning plots

ds_moc = xr.open_mfdataset(f'{path_data}/outdata/*oce*moc*.nc')

da_amoc = ds_moc.atlantic_moc.isel(time=slice(0,12)).mean(dim='time', keep_attrs=True).compute()

da_amoc.pyic.plot_sec(section='moc', clim=24, xlim=[-30,80])

# ## Mixture of plots

# +
P = pyic.Plot(2, 2, 
              projection=[ccrs.PlateCarree(), ccrs.PlateCarree(), None, None],
              sharex=False, sharey=False)

P.next()
P.plot(ds.to.isel(depth=0, time=0), clim=[-2, 32], cmap='inferno')

P.next()
P.plot(ds.so.isel(depth=0, time=0), clim=[33,37], cmap='viridis')

P.next()
P.plot_sec(ds.to.isel(time=0), clim=[-2, 32], cmap='inferno', section='170W')

P.next()
P.plot_sec(ds.so.isel(time=0), clim=[34.5, 35.5], cmap='viridis', section='170W')

# +
P = pyic.Plot(2, 2, 
              projection=[None, None, None, None],
              sharex=False, sharey=False)

P.next()
P.plot_sec(ds.to.isel(time=0), clim=[-2, 32], cmap='inferno', section='170W')

P.next()
P.plot_sec(ds.to.isel(time=0), clim=[-2, 32], cmap='inferno', section='gzave')

P.next()
P.plot_sec(ds.to.isel(time=0), clim=[-2, 32], cmap='inferno', section='azave', fpath_fx=fpath_fx)

P.next()
P.plot_sec(ds.to.isel(time=0), clim=[-2, 32], cmap='inferno', section='ipzave', fpath_fx=fpath_fx)
# -

# ## Timeseries

ds_ts = xr.open_mfdataset(f'{path_data}/outdata/*oce*mon*.nc')

# +
P = pyic.Plot(1,1, plot_cb=False)

ax, cax = P.next()
ds_ts.amoc26n.plot(marker='.')
ds_ts.amoc26n.resample(time='1Y').mean().plot()
ax.grid(True)
# -


