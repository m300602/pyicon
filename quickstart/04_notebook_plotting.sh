#!/bin/bash

echo "************************************************************" 
echo " 04_notebook_plotting.sh "
echo "************************************************************" 

path_data=`cat path_data.txt`

#echo "--- Jupytext: Generate notebook."
#jupytext --to notebook notebook_plotting.py \
#  -o ${path_data}/pyicon_output/notebook_plotting.ipynb
 
echo "--- Copy notebook."
cp notebook_plotting.ipynb ${path_data}/pyicon_output/notebook_plotting.ipynb

echo "--- Executing notebook."
jupyter nbconvert --execute  --to html ${path_data}/pyicon_output/notebook_plotting.ipynb \
  --output-dir ${path_data}/pyicon_output 
