import numpy as np
from netCDF4 import Dataset
import sys, os
from importlib import reload

import pyicon as pyic
reload(pyic)

ts = pyic.timing([0], 'start')

with open('path_data.txt') as f:
  path_data = f.read()[:-1]

tgname        = 'r2b4_oce_r0004'
gname         = 'icon_grid_0036_R02B04_O'
path_grid     = f'{path_data}/{tgname}/'
path_tgrid    = f'{path_data}/icon_grid_0036_R02B04_O/'

fname_tgrid   = f'{gname}.nc'
#path_ckdtree  = f'{path_data}/{tgname}/ckdtree/'
path_rgrid    = f'{path_data}/{tgname}/ckdtree/rectgrids/' 
path_sections = f'{path_data}/{tgname}/ckdtree/sections/' 

fpath_tgrid_source = f'{path_tgrid}/{fname_tgrid}'
fpath_tgrid_target = f'{path_grid}/{tgname}_tgrid.nc'
fpath_fx_source = f'{path_tgrid}/{gname}_fx.nc'
fpath_fx_target = f'{path_grid}/{tgname}_L40_fx.nc'

#print(fpath_tgrid_source)
#print(fpath_tgrid_target)
#print(fpath_fx_source)
#print(fpath_fx_target)

all_grids = [
  'global_1.0',
  'global_0.3',
            ]

all_secs = [
  '30W_300pts',
  '170W_300pts',
            ]

#all_grids = []
#all_secs = []

gnames = [gname]

# --- create some paths
if not os.path.exists(path_rgrid): 
  os.makedirs(path_rgrid)
if not os.path.exists(path_sections): 
  os.makedirs(path_sections)
# --- links grid file and fx file
if not os.path.exists(fpath_tgrid_target):
  os.symlink(fpath_tgrid_source, fpath_tgrid_target)
if not os.path.exists(fpath_fx_target):
  os.symlink(fpath_fx_source, fpath_fx_target)

for gname in gnames:
  ts = pyic.timing(ts, gname)
  print(gname)

  # --- grids
  sname = 'global_1.0'
  if sname in all_grids:
    pyic.ckdtree_hgrid(lon_reg=[-180.,180.], lat_reg=[-90.,90.], res=1.0,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_rgrid,
                      sname = sname,
                      gname = gname,
                      tgname = tgname,
                      )
  
  sname = 'global_0.3'
  if sname in all_grids:
    pyic.ckdtree_hgrid(lon_reg=[-180.,180.], lat_reg=[-90.,90.], res=0.3,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_rgrid,
                      sname = sname,
                      gname = gname,
                      tgname = tgname,
                      )
  
  # --- sections
  sname = '30W_300pts'
  if sname in all_secs:
    dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[-30,-80], p2=[-30,80], npoints=300,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_sections,
                      sname = sname,
                      gname = gname,
                      tgname = tgname,
                      )
    
  sname = '170W_300pts'
  if sname in all_secs:
    dckdtree, ickdtree, lon_sec, lat_sec, dist_sec = pyic.ckdtree_section(p1=[-170,-80], p2=[-170,80], npoints=300,
                      fname_tgrid  = fname_tgrid,
                      path_tgrid   = path_tgrid,
                      path_ckdtree = path_sections,
                      sname = sname,
                      gname = gname,
                      tgname = tgname,
                      )

print('make_ckdtree.py: All done!')
ts = pyic.timing(ts, 'All done!')
